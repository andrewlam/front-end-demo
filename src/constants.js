export const DATA_ENDPOINT = 'https://experimentation.getsnaptravel.com/interview/hotels';

export const PROVIDERS = {
  SNAP_TRAVEL: 'snaptravel',
  HOTELS: 'retail'
};

export const SORT_TYPES = {
  PRICE: 'price',
  RATING: 'rating',
  SAVINGS: 'savings'
};