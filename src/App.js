import React, { useState } from 'react';
import { connect } from 'react-redux';

import './App.css';
import { PROVIDERS, SORT_TYPES } from './constants';
import {
  getHotels,
  sortByPrice,
  sortByRating,
  sortBySavings
} from './store/actions';
import { Banner } from './components/Banner';
import { ActionGroup } from './components/ActionGroup';
import { Hotel } from './components/Hotel';
import { Input } from './components/Input';
import { Button } from './components/Button';
import { Content } from './components/Content';

const App = ({ getHotels, sortByPrice, sortByRating, sortBySavings, hotels, sortType }) => {
  const [city, onCityChange] = useState('');
  const [checkin, onCheckinChange] = useState('');
  const [checkout, onCheckoutChange] = useState('');

  return (
    <div className="App">
      <Banner />
      <Content>
        <ActionGroup className="flex flex-column flex-row-l">
          <Input className="f3" placeholder="Search By City" onChange={e => onCityChange(e.target.value)} />
          <div className="flex mt4 justify-between block-l mt0-l">
            <Input className="flex-auto w4 f3 ml4-l" placeholder="Check-In" onChange={e => onCheckinChange(e.target.value)} />
            <Input className="flex-auto w4 ml4 f3 ml4-l" placeholder="Check-Out" onChange={e => onCheckoutChange(e.target.value)} />
            <Button className="w4 ml4 justify-center w3-l" onClick={() => getHotels(city, checkin, checkout)}>
              <img className="h1" src="/images/search-icon.png" alt="search" />
            </Button>
          </div>
        </ActionGroup>
        <ActionGroup className="flex flex-column flex-row-l mt4 mt0-l">
          <Button className={`flex-auto mt4 justify-between items-center ${sortType === SORT_TYPES.PRICE && 'bg-light-silver'}`} onClick={sortByPrice}>
            Price
          <img className="h2" src="/images/filter-icon.png" alt="filter" />
          </Button>
          <Button className={`flex-auto mt4 ml3-l ${sortType === SORT_TYPES.RATING && 'bg-light-silver'}`} onClick={sortByRating}>Rating</Button>
          <Button className={`flex-auto mt4 ml3-l ${sortType === SORT_TYPES.SAVINGS && 'bg-light-silver'}`} onClick={sortBySavings}>Savings</Button>
        </ActionGroup>
        {hotels.map((hotel, i) => <Hotel key={i} hotel={hotel} />)}
      </Content>
    </div>
  );
};

const mapStateToProps = ({ hotels, sortType }) => ({ hotels, sortType });
const mapDispatchToProps = dispatch => ({
  getHotels: (city, checkin, checkout) => dispatch(getHotels(city, checkin, checkout, PROVIDERS.SNAP_TRAVEL)),
  sortByPrice: () => dispatch(sortByPrice()),
  sortByRating: () => dispatch(sortByRating()),
  sortBySavings: () => dispatch(sortBySavings()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
