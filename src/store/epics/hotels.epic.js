import { forkJoin, of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { filter, map, switchMap, catchError, withLatestFrom } from 'rxjs/operators';

import { GET_HOTELS, SORT_HOTELS_BY_PRICE, SORT_HOTELS_BY_RATING, SORT_HOTELS_BY_SAVINGS } from '../actionTypes';
import { getHotelsSuccess, getHotelsError, sortHotelsList } from '../actions';
import { mapHotelsResponseToModel } from '../utils';
import { DATA_ENDPOINT, SORT_TYPES, PROVIDERS } from '../../constants';
import { sortByPrice, sortByRating, sortBySavings } from './utils';

export const hotelsEpic = action$ => action$.pipe(
  filter(action => action.type === GET_HOTELS),
  switchMap(action => forkJoin({
    snapTravel: ajax.post(DATA_ENDPOINT, { ...action.payload, provider: PROVIDERS.SNAP_TRAVEL }),
    retail: ajax.post(DATA_ENDPOINT, { ...action.payload, provider: PROVIDERS.HOTELS })
  }).pipe(
    map(({ snapTravel, retail }) => getHotelsSuccess(mapHotelsResponseToModel(snapTravel, retail))),
    catchError(err => of(getHotelsError(err)))
  ))
);

export const sortHotelsByPriceEpic = (action$, store$) => action$.pipe(
  filter(action => action.type === SORT_HOTELS_BY_PRICE),
  withLatestFrom(store$),
  map(([action, store]) => ({
    hotels: sortByPrice(store.hotels),
    store
  })),
  map(({ hotels, store }) => sortHotelsList(hotels, store.hotels.length && SORT_TYPES.PRICE)),
);

export const sortHotelsByRatingEpic = (action$, store$) => action$.pipe(
  filter(action => action.type === SORT_HOTELS_BY_RATING),
  withLatestFrom(store$),
  map(([action, store]) => ({
    hotels: sortByRating(store.hotels),
    store
  })),
  map(({ hotels, store }) => sortHotelsList(hotels, store.hotels.length && SORT_TYPES.RATING)),
);

export const sortHotelsBySavingsEpic = (action$, store$) => action$.pipe(
  filter(action => action.type === SORT_HOTELS_BY_SAVINGS),
  withLatestFrom(store$),
  map(([action, store]) => ({
    hotels: sortBySavings(store.hotels),
    store
  })),
  map(({ hotels, store }) => sortHotelsList(hotels, store.hotels.length && SORT_TYPES.SAVINGS)),
);