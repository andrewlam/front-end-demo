import { sortBy } from 'lodash';

export const sortByPrice = hotels => sortBy(hotels, hotel => hotel.price);
export const sortByRating = hotels => sortBy(hotels, hotel => hotel.reviews).reverse();
export const sortBySavings = hotels => sortBy(hotels, hotel => hotel.savings).reverse();