import { combineEpics } from 'redux-observable';
import {
  hotelsEpic,
  sortHotelsByPriceEpic,
  sortHotelsByRatingEpic,
  sortHotelsBySavingsEpic
} from './hotels.epic';

const hotelEpics = [
  hotelsEpic,
  sortHotelsByPriceEpic,
  sortHotelsByRatingEpic,
  sortHotelsBySavingsEpic
];

export const rootEpic = combineEpics(...hotelEpics);