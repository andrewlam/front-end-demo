import { intersectionBy, find, gte, subtract } from 'lodash';

export const createAction = (type, payload) => ({ type, payload });

export const createRequestBody = (city, checkin, checkout) => ({
  city,
  checkin,
  checkout,
});

export const unwrapAjaxResponse = ajaxResponse => ajaxResponse.response;

export const getDifference = (number1, number2) => {
  const isFirstGreater = gte(number1, number2);
  return isFirstGreater ? subtract(number1, number2) : subtract(number2, number1);
};

export const mapAmenitiesResponseToModel = amenitiesResponse => [...amenitiesResponse];

export const mapHotelResponseToModel = hotel => ({
  address: hotel.address,
  amenities: mapAmenitiesResponseToModel(hotel.amenities),
  hotelName: hotel.hotel_name,
  id: hotel.id,
  imageUrl: hotel.image_url,
  reviews: hotel.num_reviews,
  stars: hotel.num_stars,
  price: hotel.price
});

export const mapHotelsResponseToModel = (snapTravelAjaxRes, retailAjaxRes) => {
  const snapTravelHotels = unwrapAjaxResponse(snapTravelAjaxRes).hotels;
  const retailHotels = unwrapAjaxResponse(retailAjaxRes).hotels;
  const commonHotelsById = intersectionBy(snapTravelHotels, retailHotels, 'id');

  return commonHotelsById.map(hotel => {
    const retailPrice = find(retailHotels, retailHotel => retailHotel.id === hotel.id).price;
    return {
      ...mapHotelResponseToModel(hotel),
      retailPrice,
      savings: getDifference(hotel.price, retailPrice)
    };
  }, commonHotelsById);
};