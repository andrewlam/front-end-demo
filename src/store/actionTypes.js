export const GET_HOTELS = '[Hotels] GET HOTELS';
export const GET_HOTELS_SUCCESS = '[Hotels] GET HOTELS SUCCESS';
export const GET_HOTELS_ERROR = '[Hotels] GET HOTELS ERROR';
export const SORT_HOTELS_BY_PRICE = '[Hotels] SORT HOTELS BY PRICE';
export const SORT_HOTELS_BY_RATING = '[Hotels] SORT HOTELS BY RATING';
export const SORT_HOTELS_BY_SAVINGS = '[Hotels] SORT HOTELS BY SAVINGS';
export const SORT_HOTELS_LIST = '[Hotels] SORT HOTELS LIST';
