import { createAction, createRequestBody } from './utils';
import {
  GET_HOTELS,
  GET_HOTELS_SUCCESS,
  GET_HOTELS_ERROR,
  SORT_HOTELS_BY_PRICE,
  SORT_HOTELS_LIST,
  SORT_HOTELS_BY_RATING,
  SORT_HOTELS_BY_SAVINGS
} from './actionTypes';

export const getHotels = (city, checkin, checkout) => createAction(GET_HOTELS, createRequestBody(city, checkin, checkout));
export const getHotelsSuccess = hotels => createAction(GET_HOTELS_SUCCESS, hotels);
export const getHotelsError = error => createAction(GET_HOTELS_ERROR, error);
export const sortByPrice = () => createAction(SORT_HOTELS_BY_PRICE, null);
export const sortByRating = () => createAction(SORT_HOTELS_BY_RATING, null);
export const sortBySavings = () => createAction(SORT_HOTELS_BY_SAVINGS, null);
export const sortHotelsList = (hotels, sortType) => createAction(SORT_HOTELS_LIST, { hotels, sortType });