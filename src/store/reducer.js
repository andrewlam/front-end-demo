import {
  GET_HOTELS,
  GET_HOTELS_SUCCESS,
  GET_HOTELS_ERROR,
  SORT_HOTELS_LIST
} from './actionTypes';

const INITIAL_APP_STATE = {
  pending: false,
  error: false,
  hotels: [],
  activeSort: ''
};

export default function reducer(state = INITIAL_APP_STATE, action) {
  switch (action.type) {
    case GET_HOTELS: {
      return {
        ...state,
        pending: true,
        error: false
      };
    }

    case GET_HOTELS_SUCCESS: {
      return {
        ...state,
        pending: false,
        error: false,
        hotels: action.payload
      };
    }

    case GET_HOTELS_ERROR: {
      return {
        ...state,
        pending: false,
        error: true
      };
    }

    case SORT_HOTELS_LIST: {
      return {
        ...state,
        pending: false,
        error: false,
        hotels: action.payload.hotels,
        sortType: action.payload.sortType
      };
    }

    default: {
      return { ...state };
    }
  }
}
