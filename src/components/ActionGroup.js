import React from 'react';

export const ActionGroup = ({ children, className }) => <div className={className}>
  {children}
</div>;