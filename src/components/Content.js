import React from 'react';

export const Content = ({ children }) => <div className="pa4">{children}</div>;