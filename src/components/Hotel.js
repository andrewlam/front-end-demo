import React from 'react';

import { Button } from './Button';

const HotelPreview = ({ hotel, className }) =>
  <div className={`flex ${className}`}>
    <div className="flex relative">
      {hotel.savings > 0 && <div className="absolute bg-red flex flex-column w3 h2 top-1 br2 justify-center items-center">
        <div className="white">${hotel.savings}</div>
        <div className="white">OFF</div>
      </div>}
      <img className="h4" src={hotel.imageUrl} alt="hotel preview" />
    </div>
  </div>;


const HotelInfo = ({ hotel, className }) =>
  <div className={`flex flex-column ml2 flex-auto ${className}`}>
    <span className="avenir f3">{hotel.hotelName}</span>
    <span className="dn db-l mt2">{hotel.address}</span>
    <div className="flex mt2">
      <div className="flex flex-column flex-auto">
        <span className="avenir f5">{hotel.stars} Star</span>
        <span className="avenir f5 mt2">{hotel.reviews} Reviews</span>
      </div>
      <div className="flex flex-column flex-auto">
        {hotel.amenities.map((amenity, i) => <span className="avenir f4" key={i}>{amenity}</span>)}
      </div>
    </div>
  </div>;

const HotelPricing = ({ hotel }) =>
  <React.Fragment>
    <div className="flex flex-auto flex-column b--red ba justify-center items-center pa3 br2">
      <span className={`f3 avenir red ${hotel.savings > 0 && 'strike'}`}>USD ${hotel.retailPrice}</span>
      <img className="h3" src="/images/hotels-logo.png" alt="" />
    </div>
    <div className="flex flex-auto flex-column b--green ba justify-center items-center ml2 br2">
      <span className="f3 avenir green">USD ${hotel.price}</span>
      <Button className="bg-green white">View Details</Button>
    </div>
  </React.Fragment>;

export const Hotel = ({ hotel }) =>
  <div className="shadow-1 pa3 mt4 flex-column flex flex-row-l items-center-l">
    <div className="flex w-50-l">
      <HotelPreview className="w-50-l" hotel={hotel} />
      <HotelInfo className="w-50-l" hotel={hotel} />
    </div>
    <div className="flex mt1 w-50-l">
      <HotelPricing hotel={hotel} />
    </div>
  </div>;