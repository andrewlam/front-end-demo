import React from 'react';

export const Button = ({ className, ...props }) => <button className={`flex avenir f3 shadow-1 b--none br2 pa3 ${className}`} {...props}>{props.children}</button>;