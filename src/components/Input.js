import React from 'react';

export const Input = ({ className, ...props }) => <input className={`shadow-1 b--none br2 pa3 f2 avenir ${className}`} {...props} />;