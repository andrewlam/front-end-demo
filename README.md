# Frontend Demo

This is our front-end task for potential new engineers. We expect it to require two to three hours of work. If you are time constrained, please let us know how much time you have allocated to this and how much you were able to get done.

The task is to implement a responsive web application that follows the design and functionality described below. We've set up boilerplate for React and Redux using `create-react-app`. As long as you use React and Redux, you are welcome to modify the setup or add and remove libraries as you see fit. Make sure you have `node` and `npm` installed and run ```npm install``` and then ```npm start``` to start your application. To run tests you can run ```npm test```. Additional unit tests are optional.

Please fork this repo and make a starting commit with the message 'start'. Commit your finished code and push to your forked repo when you are finished. If you require technical assistance please contact brendan@snaptravel.com or cory@snaptravel.com.

Thanks! 😄


### Design

The following screenshots show the mobile and desktop layouts for a responsive web application. You are asked to build and style the header, input elements, and filter buttons as shown and in a responsive manner. The necessary image assets can be found in the `public/images` directory. The visuals for the hotel details are provided for reference only; you may render the hotel data (see next section) in any convenient manner.

**Mobile**
![](https://cdn.zeplin.io/59dcef779ee5b755ceb376f3/screens/FE1DFBA7-3235-4B28-BAAA-B8C4F3AFC56A.png)

**Desktop**
![](https://cdn.zeplin.io/59dcef779ee5b755ceb376f3/screens/076AC023-A27D-488C-8E85-698745374675.png)


### Functionality

**Step 1**

Only the following elements should be visible on first load.

- Header
- City input
- Checkin input (YYYY-MM-DD format)
- Checkout input (YYYY-MM-DD format)
- Search button

The three inputs accept strings. Do not worry about form validation, datepicker, or autocomplete functionality.

**Step 2**

When the Search button is clicked, make **2 HTTP POST requests** in parallel to 'https://experimentation.getsnaptravel.com/interview/hotels' with the following request body

```
{
  city : city_string_input,
  checkin : checkin_string_input,
  checkout : checkout_string_input,
  provider : 'snaptravel'
}
```

1) This above return SnapTravel rates for hotels in the city (the rate shown on the right and in green in the design)


```
{
  city : city_string_input,
  checkin : checkin_string_input,
  checkout : checkout_string_input,
  provider : 'retail'
}
```

2) This above return Hotels.com rates for hotels in the city (the rate shown on the left and in red in the design)

The responses will be a json which has an array of hotels and prices.
```
[{
  id : 12,
  hotel_name : 'Center Hilton',
  num_reviews : 209,
  address : '12 Wall Street, Very Large City',
  num_stars : 4,
  amenities : ['Wi-Fi', 'Parking'],
  image_url : 'https://images.trvl-media.com/hotels/1000000/20000/19600/19558/19558_410_b.jpg',
  price : 132.11
},
...
]
```

After both these calls have returned take **only** the hotels that appear in both the responses to render the list of hotels and prices as per the design.

For example, if the first call returned hotels with id [1,2] with SnapTravel prices 110 and 210 and the second call returned hotels [2,3] with Hotels.com prices 250 and 350 respectively, you would only render hotel 2 in the list with a Hotels.com price of 250 and a SnapTravel price of 210, as shown below:

![](https://i.imgur.com/s1pIdcy.png)


**Step 3**

The list of hotels should be sorted based on the currently selected  sorting type (default to sort by price, lowest first).

Users should be able to click on a different sorting type to change the sorting order of the list of results.

The sorting logic of the three types is as follows:

- Price - Sort by the SnapTravel price of the hotel, lowest prices first
- Rating - Sort by the number of reviews, greatest number of reviews first
- Savings - Sort by the difference between the Hotels.com price and the SnapTravel price, largest differences first

The currently selected sorting type should highlighted as shown in the design.
